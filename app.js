const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const path = require('path');
const cors = require('cors');
const passport = require('passport');


const app = express();

app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(bodyParser.json());

app.use(cors());

// set up the static directory
app.use(express.static(path.join(__dirname, 'public')));

// passport Middleware
app.use(passport.initialize());
require('./config/passport')(passport);

// connect with the database
const uri = require('./config/keys').mongoURI;
mongoose.connect(uri, {
    useNewUrlParser: true
}).then(() => {
    console.log(`Database connected successfully ${uri}`)
}).catch(err => {
    console.log(`Unable to connect with the database ${err}`)
});

// Users route
const users = require('./routes/api/users');
app.use('/api/users', users);

// Task route
const task = require('./routes/api/task');
app.use('/api/task', task);

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'public/index.html'));
})

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => {
    console.log(`Server started on port ${PORT}`);
})